var path = require('path');

var BUILD_DIR = path.join(__dirname, "dist");

module.exports = {
  entry: './src/index.js',
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: BUILD_DIR,
    compress: true,
    port: 9000
  }
};
