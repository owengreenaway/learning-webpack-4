# Initial motivation

At the moment webpack is a bit of a magical black box. I want to understand it better.

# Learnings

- webpack crawls from entry point
- webpack uses defaults so is technically doesn't need configuring if your project matched the defaults
- transpiles es6 modules to require/commonJS (clarification needed)
- either use Babel for ES6+ to ES5/ES3 or TypeScript to ES5/ES3, not both
- assets are crawled by webpack and loaded using loaders. Small assets are inlined, larger ones are dynamically copied to dist folder
- due to dynamic file names (hashes, code splitting) index.html also needs to be dynamically generated. (Can use template)
- image minification is applied during the loaders/plugins (clarification needed)
- dist isn't deleted/cleaned by default
- webpack-dev-server doesn't actually build the files to your file system, only memory

# Useful documentation

- [webpack](https://webpack.js.org/)
- [webpack cli](https://webpack.js.org/api/cli/)
- [serve](https://github.com/zeit/serve)
- [list of npm scripts](https://docs.npmjs.com/misc/scripts)
- [zero config tutorial blog](https://www.valentinog.com/blog/webpack-4-tutorial/)
- [tsconfig.json](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html)
- [react+webpack+typescript](https://www.typescriptlang.org/docs/handbook/react-&-webpack.html)
- [need to read](https://medium.com/webpack/webpack-4-code-splitting-chunk-graph-and-the-splitchunks-optimization-be739a861366)
- [webpack 4 defaults blog](https://medium.com/webpack/webpack-4-mode-and-optimization-5423a6bc597a)