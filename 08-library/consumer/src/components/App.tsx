import * as React from "react";
import { Hello, Logo } from "08-library";

export class App extends React.Component<{}, {}> {

  render() {
    console.log('App');
    return (
      <div>
        <h1>Hello from app!</h1>
        <Hello compiler='string 1' framework='string 2' />
        <Logo />
      </div>
    );
  }
}