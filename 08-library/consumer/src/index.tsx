import * as React from "react";
import * as ReactDOM from "react-dom";

import { App } from "./components/App";
import greet from './helper';

greet('Owen');

ReactDOM.render(
    <App />,
    document.getElementById("example")
);