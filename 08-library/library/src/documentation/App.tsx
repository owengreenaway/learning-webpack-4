import * as React from "react";
import { Hello, Logo } from '../index';

export class App extends React.Component<{}, {}> {
  render() {
    console.log('App');
    return (
      <div>
        <h1>Documentation</h1>
        <h2>Hello component:</h2>
        <Hello compiler="string 1" framework="string 2"/>
        <h2>Logo component:</h2>
        <Logo />
      </div>
    );
  }
}