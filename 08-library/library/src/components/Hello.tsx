
import * as React from "react";

export interface HelloProps { compiler: string; framework: string; }

export default class Hello extends React.Component<HelloProps, {}> {

  render() {
    console.log('Hello World');
    return (
      <p>Hello from {this.props.compiler} and {this.props.framework}!</p>
    );
  }
}