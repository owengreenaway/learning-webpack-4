/// <reference types="react" />
import * as React from "react";
export interface HelloProps {
    compiler: string;
    framework: string;
}
export default class Hello extends React.Component<HelloProps, {}> {
    render(): JSX.Element;
}
