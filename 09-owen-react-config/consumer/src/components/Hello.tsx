/// <reference path='./Hello.d.ts'/>

import * as React from "react";
import * as feather from '../images/feather.png';
import * as weight from '../images/weight.png';

export interface HelloProps { compiler: string; framework: string; }

// 'HelloProps' describes the shape of props.
// State is never set so we use the '{}' type.
export class Hello extends React.Component<HelloProps, {}> {

  render() {
    console.log('Hello World');
    return (
      <div>
        <h1>Hello from {this.props.compiler} and {this.props.framework}!</h1>
        <p>Small image:</p>
        <img src={feather} alt="feather" />
        <p>Large image</p>
        <img src={weight} alt="weight" />
      </div>
    );
  }
}